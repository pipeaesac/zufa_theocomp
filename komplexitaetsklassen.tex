
\section{Komplexitätsklassen}

\begin{breakbox}
\textbf{Spezifikation:}
\newline In der Komplexitätstheorie bezeichnet eine Komplexitätsklasse eine Menge von Problemen, welche sich in einem ressourcenbeschränkten Berechnungsmodell berechnen lassen. Eine Komplexitätsklasse ist durch 4 Parameter spezifiziert:
\begin{enumerate}
	\item Berechnungsmodell: Art der Maschine, mit welcher Berechnung durchgeführt wird (1-Band-TM, k-Band-TM, etc.).
	\item Berechnungsmodus: deterministisch / nicht deterministisch? Wie kommt Maschine zum Schluss?
	\item Betrachtete Ressourcen (Zeit, Platz, etc.).
	\item Schranke für die betrachteten Ressourcen ($n^k, 2^n, n log(n)$, etc.).
\end{enumerate}
Was braucht wie wie viel von was?

Beispiel: $L = \{0^k 1^k | k \in \mathbb{N}\}$ gehört in die Klasse der in quadratischer (4) Zeit (3) mit einer deterministischen (2) 1-Band-TM (1) entscheidbaren Sprachen.
\end{breakbox}

\begin{breakbox}
\textbf{Notation:}
\begin{itemize}
	\item $\mathcal{T}\mathcal{M}_\Sigma^1$: Menge der deterministischen 1-Band-TM über Alphabet $\Sigma$.
	\item $\mathcal{T}\mathcal{M}_\Sigma$: Menge der deterministischen Mehrband-TM über Alphabet $\Sigma$ (Es gilt: $\mathcal{T}\mathcal{M}_\Sigma^1 \subset \mathcal{T}\mathcal{M}_\Sigma$).
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Zeitkomplexität:}
\begin{itemize}
	\item $DTIME_1(f(n)) := \{L \subset \Sigma^* | \exists M \in \mathcal{T}\mathcal{M}_\Sigma^1 : L = L(M)$ und $T_M(n) = \mathcal{O}(f(n))\}$.
	\item $DTIME(f(n)) := \{L \subset \Sigma^* | \exists M \in \mathcal{T}\mathcal{M}_\Sigma : L = L(M)$ und $T_M(n) = \mathcal{O}(f(n))\}$.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Platzkomplexität:}
\begin{itemize}
	\item $DSPACE_1(f(n)) := \{L \subset \Sigma^* | \exists M \in \mathcal{T}\mathcal{M}_\Sigma^1 : L = L(M)$ und $S_M(n) = \mathcal{O}(f(n))\}$.
	\item $DSPACE(f(n)) := \{L \subset \Sigma^* | \exists M \in \mathcal{T}\mathcal{M}_\Sigma : L = L(M)$ und $S_M(n) = \mathcal{O}(f(n))\}$.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Beispiel:}
\newline Für die Sprache $L = \{0^k 1^k | k \in \mathbb{N}\}$ gilt: $L \in DTIME_1(n^2), L \in DTIME(n)$ und $L \in DSPACE_1(n)$.
\end{breakbox}

\begin{breakbox}
\textbf{These von Cobham und Edmonds:}
\newline Wenn zwei universelle Berechnungsmodelle (mit logarithmischem Kostenmass) gegeben sind, dann existiert ein Polynom $p(n)$, so dass $t$ Schritte im ersten Modell durch $p(t)$ Schritte im zweiten Modell simuliert werden können.
\newline Logarithmisches Kostenmass: Grösse des Inputs, der aus natürlichen Zahlen besteht, wird mit Hilfe einer $m$-adischen Darstellung gemessen. Anzahl Buchstaben für $m$-adische Darstellung einer Zahl $n$: $|n|_m = [log_m(n)] + 1$.
\end{breakbox}

\begin{breakbox}
\textbf{Klasse P:}
\begin{center}
	$P := \bigcup_{k>0} DTIME(n^k)$
\end{center}
ist die Klasse der in polynomialer Laufzeit, deterministisch entscheidbaren Sprachen, d.\,h. $L \in P \Leftrightarrow \exists M \in \mathcal{T} \mathcal{M}_\Sigma, k \in \mathbb{N}^* : L = L(M)$ und $T_M(n) = \mathcal{O}(n^k)$.
\end{breakbox}

\begin{breakbox}
\textbf{Bemerkung:}
\newline Seien $L_1, L_2 \in P$ mit $\Sigma := \{0,1\}$. Dann gilt:
\begin{itemize}
	\item $(L_1 \cup L_2) \in P$.
	\item $(L_1 \cdot L_2) \in P$.
	\item $(\Sigma^* - L_1) \in P$.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Notation:}
\begin{itemize}
	\item $\mathcal{NTM}_\Sigma$: Menge der nicht deterministischen Mehrband-TM über Alphabet $\Sigma$.
	\item $NTIME(f(n)) := \{L \subset \Sigma^* | \exists M \in \mathcal{NTM}_\Sigma : L = L(M)$ und $T_M(n) = \mathcal{O}(f(n))\}$.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Klasse NP:}
\begin{center}
	$NP := \bigcup_{k>0} NTIME(n^k)$
\end{center}
ist die Klasse der in polynomialer Zeit mit einer nicht-deterministischen Turing-Maschine entscheidbaren Sprachen, d.\,h. $L \subset \Sigma^*$ liegt in NP, wenn gilt: Es existiert eine deterministische TM $M$ mit polynomialer Laufzeit und ein Polynom $p$, so dass für alle $x \in \Sigma^*$ gilt:
\begin{center}
	$x \in L \Leftrightarrow \exists u \in \Sigma^*$ mit $|u| \leq p(|x|)$ und $M(x, u) = 1$.
\end{center}
Ein zu $x$ gehöriges Wort $u$ mit $M(x, u) = 1$ heisst Zertifikat für $x$.
\newline Merke: P $\subseteq$ NP. Die Bedingung oben ist mit $u = \varepsilon$ erfüllt.
\end{breakbox}

\begin{breakbox}
\textbf{Bemerkung:}
\newline Seien $L_1, L_2 \in NP$ mit $\Sigma := \{0,1\}$. Dann gilt:
\begin{itemize}
	\item $(L_1 \cup L_2) \in NP$.
	\item $(L_1 \cdot L_2) \in NP$.
	\item $(\Sigma^* - L_1)$ ist nicht bekannt.
\end{itemize}
\end{breakbox}
