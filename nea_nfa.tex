
\section{NFA (NEA)}

\begin{breakbox}
\textbf{Zustandsübergangsdiagramm:}
\begin{center}
\begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=1.5cm, scale = 0.75, transform shape]
\node[initial,state] 			(A)						{$q_1$};
\node[state] 						(B)	[right of=A]	{$q_2$};
\node[state] 						(C)	[right of=B]	{$q_3$};
\node[state, accepting]	(D)	[right of=C]	{$q_4$};
\path[->] 	(A) 	edge 						node {1} 						(B)
            			edge [loop above] 	node {0,1} 					(A)
         		(B) 	edge						node {0, $\varepsilon$} 	(C)
        		(C) 	edge						node {1} 						(D)
         		(D)	edge [loop right]		node {0,1} 					(D);
\end{tikzpicture}
\end{center}
\begin{itemize}
	\item Leeres Wort $\varepsilon$ kann vorkommen.
	\item Buchstaben können mehrfach vorkommen.
	\item Buchstaben müssen nicht immer vorkommen.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Potenzmenge:}
\begin{itemize}
	\item $\mathcal{P}(M) = 2^M$ ist Menge aller Teilmengen von $M$.
	\item $\mathcal{P}(\{q_1, q_2\}) =$\\$\{ \emptyset, \{q_1\}, \{q_2\}, \{q_1, q_2\}\}$.
	\item $|2^M| = 2^{|M|}$.
	\item $\mathcal{P}(\emptyset) = 2^\emptyset = \{\emptyset\}$.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Definition NFA:}
\newline Ein NFA ist ein 5-Tupel $(Q, \Sigma, \delta, s, F)$ mit
\begin{enumerate}
	\item $Q$: ist endliche nichtleere Menge von Zuständen.
	\item $\Sigma$ ist Eingabealphabet.
	\item $\delta$: $Q \times \Sigma_\varepsilon \rightarrow \mathcal{P}(Q) = 2^Q$, mit $\Sigma_\varepsilon = \Sigma \cup \{\varepsilon\}$ (total).
	\item $s \in Q$ ist Startzustand (keine Menge!).
	\item $F \subseteq Q$ sind akzeptierende Zustände (Menge!).
\end{enumerate}
\end{breakbox}

\begin{breakbox}
\textbf{Verarbeitung:}
\newline Sei $N = (Q, \Sigma, \delta, s, F)$ ein NFA und $w \in \Sigma^*$. $N$ akzeptiert $w$, falls sich $w$ schreiben lässt als $w = y_1 y_2 ... y_m$ mit $y_i \in \Sigma_\varepsilon$ und eine Folge von Zuständen existiert $r_0, r_1, ..., r_m$ mit $r_i \in Q$ mit
\begin{enumerate}
	\item $r_0 = s$.
	\item $r_i \in \delta(r_{i-1}, y_i)$ für $i \in 1 ... m$.
	\item $r_m \in F$.
\end{enumerate}
\end{breakbox}

\begin{breakbox}
\textbf{Theorem:}
\newline Jeder NFA hat einen äquivalenten DFA. Äquivalent := erkennt die gleiche Sprache.
\newline
\newline \textbf{Beweis (konstruktiv):}
\newline Sei $N = (Q, \Sigma, \delta, s, F)$ ein NFA, der L erkennt. Wir konstruieren DFA $D = (Q', \Sigma, \delta', s', F')$, der L ebenfalls erkennt.
\begin{enumerate}
	\item $Q' = 2^Q$.
	\item $\delta'(R, a) = \underset{r \in R}{\cup} E(\delta(r, a))$, mit $R \in Q'$.
	\item $s' = E(\{s\})$. (d.\,h. der Startzustand des NFA + alle Zustände, die von dort mit $\varepsilon$ erreichbar sind.
	\item $F' = \{R \in Q | R \cap F = Q\}$.
\end{enumerate}
$E(R) := \{q \in Q | q$ kann von $R$ aus erreicht werden via 0 oder mehr $\varepsilon$-Übergängen.
\end{breakbox}

\begin{breakbox}
\textbf{Bemerkungen:}
\begin{enumerate}
	\item $\delta'(\emptyset, a) = \underset{r \in \emptyset}{\cup} E(\delta(r, a)) = \emptyset$. \newline Iteration über alle Elemente der leeren Menge $\emptyset$ ist wieder leere Menge. Denn diese ist das neutrale Element der Vereinigung.
	\item $\emptyset \notin F'$. Die leere Menge ist nie akzeptierender Zustand.
\end{enumerate}
\end{breakbox}

\begin{breakbox}
\textbf{Alternative Konstruktion:}
\newline Beispielautomat
\begin{center}
\begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=1.5cm, scale = 0.75, transform shape]
\node[initial,state] 			(A)							{1};
\node[state, accepting] 	(B)	[right of=A]		{2};
\node[state] 						(C)	[right of=B]		{3};
\path[->] 	(A) 	edge 	[bend left=30]		node {$\varepsilon$} 	(B)
						edge	[bend right=60]	node {a} 					(C)
         		(B) 	edge	[bend left=30]		node {a} 					(A)
         		(C)	edge								node {a, b}				(B)
         				edge	[loop right]			node {b}					(C);
\end{tikzpicture}
\end{center}
Man kann nun die Zustände, die erreichbar sind, ausgehend vom Startzustand (hier $\{1, 2\}$) abarbeiten. Das ergibt folgende Transitionsfunktion:
\begin{center}
\begin{tabular}{c|c c}
	$\delta'$ & a & b \\ \hline
	$\{1, 2\}$ & $\{1, 2, 3\}$ & $\emptyset$ \\
	$\{1, 2, 3\}$ & $\{1, 2, 3\}$ & $\{2, 3\}$ \\
	$\emptyset$ & $\emptyset$ & $\emptyset$ \\
	$\{2, 3\}$ & $\{1, 2\}$ & $\{2, 3\}$ \\
\end{tabular}
\end{center}
Der resultierende, reduzierte DFA, der immer noch die selbe Sprache erkennt, sieht folgendermassen aus:
\begin{center}
\begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=2.0cm, scale = 0.75, transform shape]
\node[initial,state, accepting] 	(A)							{$\{1, 2\}$};
\node[state] 								(B)	[below of=A]		{$\{2, 3\}$};
\node[state] 								(C)	[right of=A]		{$\emptyset$};
\node[state, accepting]			(D)	[right of = B]		{$\{1, 2, 3\}$};
\path[->] 	(A) 	edge 								node {b}				 	(C)
						edge								node {a} 					(D)
         		(B) 	edge								node {a} 					(A)
         				edge	[loop left]				node {b}					(B)
         		(C)	edge	[loop right]			node {a, b}				(C)
         		(D)	edge	[loop right]			node {a}					(D)
         				edge								node {b}					(B);
\end{tikzpicture}
\end{center}
Es gilt:
\begin{enumerate}
	\item $Q' = \{\{1, 2\}, \{1, 2, 3\}, \emptyset, \{2, 3\}\}$.
	\item $\Sigma = \{a, b\}$.
	\item $\delta' =$ siehe oben.
	\item $s' = \{1, 2\}$.
	\item $F' = \{\{1, 2\}, \{1, 2, 3\}\}$.
\end{enumerate}
\end{breakbox}

\begin{breakbox}
\textbf{Theorem:}
\newline Eine Sprache ist regulär, genau dann, wenn sie von einem NFA erkannt wird.
\end{breakbox}
